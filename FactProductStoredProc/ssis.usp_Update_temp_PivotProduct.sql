CREATE OR ALTER  PROCEDURE temp.usp_Update_temp_PivotProduct

AS

BEGIN

    DROP TABLE IF EXISTS temp.PivotProduct;
    CREATE TABLE temp.PivotProduct (
        Date DATE,
        SaleAdjustmentDate DATE,
        ItemID INT,
        SupplyChainStatusNodeID NVARCHAR(16),
        ItemStatus INT,
        InvStatus INT,
        ProcurementStatus INT,
        SaleOOSEligible BIT,
        DaysSinceRCVOOSEligible BIT,
        InventoryStatusOOSEligible BIT,
        OOSEligible BIT,
        [Network - Daily Unit Sales] INT,
        PureRankS30 INT,
        PureRankS90 INT,
        PureRankS365 INT,
        ScaledRankS30 INT,
        ScaledRankS90 INT,
        ScaledRankS365 INT,
        PureTop200SKUS30 BIT,
        PureTop200SKUS90 BIT,
        PureTop200SKUS365 BIT,
        ScaledTop200SKUS30 BIT,
        ScaledTop200SKUS90 BIT,
        ScaledTop200SKUS365 BIT,
        SKURankNodeID AS CAST(
            CONCAT(
                PureTop200SKUS30, '::',
                PureTop200SKUS90, '::',
                PureTop200SKUS365, '::',
                ScaledTop200SKUS30, '::',
                ScaledTop200SKUS90, '::',
                ScaledTop200SKUS365
            )
        AS NCHAR(22)),
        S30 INT,
        S90 INT,
        S365 INT,
        RollingTotalS30 INT,
        RollingTotalS90 INT,
        RollingTotalS365 INT,
        TotalS30 INT,
        TotalS90 INT,
        TotalS365 INT,
        ScaleFactor30 DECIMAL(38, 18),
        ScaleFactor90 DECIMAL(38, 18),
        ScaleFactor365 DECIMAL(38, 18),
        PureItemGrade30Perc DECIMAL(38, 18),
        PureItemGrade90Perc DECIMAL(38, 18),
        PureItemGrade365Perc DECIMAL(38, 18),
        ScaledItemGrade30Perc DECIMAL(38, 18),
        ScaledItemGrade90Perc DECIMAL(38, 18),
        ScaledItemGrade365Perc DECIMAL(38, 18),
        PureItemGrade30 NVARCHAR(1),
        PureItemGrade90 NVARCHAR(1),
        PureItemGrade365 NVARCHAR(1),
        ScaledItemGrade30 NVARCHAR(1),
        ScaledItemGrade90 NVARCHAR(1),
        ScaledItemGrade365 NVARCHAR(1),
        ItemGradeSpreadNode NCHAR(18),
        [Network - OOS] BIT,
        [1421 Kingbird - On Hand Inventory] INT,
        [2610 West L - On Hand Inventory] INT,
        [4021 N 27th - On Hand Inventory] INT,
        [Nevada - On Hand Inventory] INT,
        [1000 Commerce - On Hand Inventory] INT,
        [Pennsylvania - On Hand Inventory] INT,
        [Resell - On Hand Inventory] INT,
        [1001 W Upland - On Hand Inventory] INT,
        [Nevada Marion - On Hand Inventory] INT,
        [NV FC Transfers - On Hand Inventory] INT,
        [Indiana FC - On Hand Inventory] INT,
        [Nevada Lamb - On Hand Inventory] INT,
        [Parts - On Hand Inventory] INT,
        [Nevada Offsite Storage - On Hand Inventory] INT,
        [Pennsylvania Overflow - On Hand Inventory] INT,
        [Nevada Ann - On Hand Inventory] INT,
        [Georgia - On Hand Inventory] INT,
        [Texas Wintergreen - On Hand Inventory] INT,
        [NE FC Transfers - On Hand Inventory] INT,
        [GA FC Transfers - On Hand Inventory] INT,
        [TX FC Transfers - On Hand Inventory] INT,
        [IN FC Transfers - On Hand Inventory] INT,
        [PA FC Transfers - On Hand Inventory] INT,
        [Indiana Greenwood - On Hand Inventory] INT,
        [Pennsylvania Nanticoke - On Hand Inventory] INT,
        [1421 Kingbird - Net Daily Adjustments] INT,
        [2610 West L - Net Daily Adjustments] INT,
        [4021 N 27th - Net Daily Adjustments] INT,
        [Nevada - Net Daily Adjustments] INT,
        [1000 Commerce - Net Daily Adjustments] INT,
        [Pennsylvania - Net Daily Adjustments] INT,
        [Resell - Net Daily Adjustments] INT,
        [1001 W Upland - Net Daily Adjustments] INT,
        [Nevada Marion - Net Daily Adjustments] INT,
        [NV FC Transfers - Net Daily Adjustments] INT,
        [Indiana FC - Net Daily Adjustments] INT,
        [Nevada Lamb - Net Daily Adjustments] INT,
        [Parts - Net Daily Adjustments] INT,
        [Nevada Offsite Storage - Net Daily Adjustments] INT,
        [Pennsylvania Overflow - Net Daily Adjustments] INT,
        [Nevada Ann - Net Daily Adjustments] INT,
        [Georgia - Net Daily Adjustments] INT,
        [Texas Wintergreen - Net Daily Adjustments] INT,
        [NE FC Transfers - Net Daily Adjustments] INT,
        [GA FC Transfers - Net Daily Adjustments] INT,
        [TX FC Transfers - Net Daily Adjustments] INT,
        [IN FC Transfers - Net Daily Adjustments] INT,
        [PA FC Transfers - Net Daily Adjustments] INT,
        [Indiana Greenwood - Net Daily Adjustments] INT,
        [Pennsylvania Nanticoke - Net Daily Adjustments] INT,
        [1421 Kingbird - OOS] BIT,
        [2610 West L - OOS] BIT,
        [4021 N 27th - OOS] BIT,
        [Nevada - OOS] BIT,
        [1000 Commerce - OOS] BIT,
        [Pennsylvania - OOS] BIT,
        [Resell - OOS] BIT,
        [1001 W Upland - OOS] BIT,
        [Nevada Marion - OOS] BIT,
        [NV FC Transfers - OOS] BIT,
        [Indiana FC - OOS] BIT,
        [Nevada Lamb - OOS] BIT,
        [Parts - OOS] BIT,
        [Nevada Offsite Storage - OOS] BIT,
        [Pennsylvania Overflow - OOS] BIT,
        [Nevada Ann - OOS] BIT,
        [Georgia - OOS] BIT,
        [Texas Wintergreen - OOS] BIT,
        [NE FC Transfers - OOS] BIT,
        [GA FC Transfers - OOS] BIT,
        [TX FC Transfers - OOS] BIT,
        [IN FC Transfers - OOS] BIT,
        [PA FC Transfers - OOS] BIT,
        [Indiana Greenwood - OOS] BIT,
        [Pennsylvania Nanticoke - OOS] BIT
    );

    DROP TABLE IF EXISTS temp.ItemGradeCutoffs;
    CREATE TABLE temp.ItemGradeCutoffs (
        PrevCutoff DECIMAL(7,5),
        PercCutoff DECIMAL(7,5),
        Grade NCHAR(1)
    );


    INSERT INTO temp.ItemGradeCutoffs
    SELECT
        -0.01 PrevCutoff,
        0.70 PercCutoff,
        'A' Grade
    UNION ALL
    SELECT
        0.70 PrevCutoff,
        0.80 PercCutoff,
        'B' Grade
    UNION ALL
    SELECT
        0.80 PrevCutoff,
        0.90 PercCutoff,
        'C' Grade
    UNION ALL
    SELECT
        0.90 PrevCutoff,
        0.95 PercCutoff,
        'D' Grade
    UNION ALL
    SELECT
        0.95 PrevCutoff,
        10.00 PercCutoff,
        'E' Grade

    CREATE CLUSTERED INDEX temp_ItemGradeCutoffs ON temp.ItemGradeCutoffs(PrevCutoff, PercCutoff);


    INSERT INTO temp.PivotProduct
    SELECT
        sf.Date,
        rs.SaleAdjustmentDate,
        sf.ItemID,
        COALESCE(rs.SupplyChainStatusNodeID, '-1::-1::-1') SupplyChainStatusNodeID,
        COALESCE(rs.ItemStatus, '-1') ItemStatusID,
        COALESCE(rs.InvStatus, '-1') InventoryStatusID,
        COALESCE(rs.ProcurementStatus, '-1') ProcurementStatusID,
        rs.SaleOOSEligible,
        rs.DaysSinceRCVOOSEligible,
        rs.InventoryStatusOOSEligible,
        sf.OOSEligible,
        rs.[Network - Daily Unit Sales],
        sf.PureRankS30,
        sf.PureRankS90,
        sf.PureRankS365,
        ppw.ScaledRankS30,
        ppw.ScaledRankS90,
        ppw.ScaledRankS365,
        ppw.PureTop200SKUS30,
        ppw.PureTop200SKUS90,
        ppw.PureTop200SKUS365,
        ppw.ScaledTop200SKUS30,
        ppw.ScaledTop200SKUS90,
        ppw.ScaledTop200SKUS365,
        rs.S30,
        rs.S90,
        rs.S365,
        sf.RollingTotalS30,
        sf.RollingTotalS90,
        sf.RollingTotalS365,
        sf.TotalS30,
        sf.TotalS90,
        sf.TotalS365,
        sf.ScaleFactor30,
        sf.ScaleFactor90,
        sf.ScaleFactor365,
        CASE
            WHEN sf.TotalS30 != 0
                THEN CAST(sf.RollingTotalS30 AS DECIMAL(38, 18))/sf.TotalS30
            ELSE 0
        END PureItemGrade30Perc,
        CASE
            WHEN sf.TotalS90 != 0
                THEN CAST(sf.RollingTotalS90 AS DECIMAL(38, 18))/sf.TotalS90
            ELSE 0
        END PureItemGrade90Perc,
        CASE
            WHEN sf.TotalS365 != 0
                THEN CAST(sf.RollingTotalS365 AS DECIMAL(38, 18))/sf.TotalS365
            ELSE 0
        END PureItemGrade365Perc,
        CASE
            WHEN sf.TotalS30 != 0
                THEN (sf.RollingTotalS30*sf.ScaleFactor30)/sf.TotalS30
            ELSE 0
        END ScaledItemGrade30Perc,
        CASE
            WHEN sf.TotalS90 != 0
                THEN (sf.RollingTotalS90*sf.ScaleFactor90)/sf.TotalS90
            ELSE 0
        END ScaledItemGrade90Perc,
        CASE
            WHEN sf.TotalS365 != 0
                THEN (sf.RollingTotalS365*ScaleFactor365)/sf.TotalS365
            ELSE 0
        END ScaledItemGrade365Perc,
        pig30.Grade PureItemGrade30,
        pig90.Grade PureItemGrade90,
        pig365.Grade PureItemGrade365,
        sig30.Grade ScaledItemGrade30,
        sig90.Grade ScaledItemGrade90,
        sig365.Grade ScaledItemGrade365,
        CONCAT(COALESCE(pig365.Grade, 'U'), '::',
            COALESCE(sig365.Grade, 'U'), '::',
            COALESCE(pig90.Grade, 'U'), '::',
            COALESCE(sig90.Grade, 'U'), '::',
            COALESCE(pig30.Grade, 'U'), '::',
            COALESCE(sig30.Grade, 'U')
        ) ItemGradeSpreadNode,
        rs.[Network - OOS],
        wp.[1421 Kingbird - On Hand Inventory],
        wp.[2610 West L - On Hand Inventory],
        wp.[4021 N 27th - On Hand Inventory],
        wp.[Nevada - On Hand Inventory],
        wp.[1000 Commerce - On Hand Inventory],
        wp.[Pennsylvania - On Hand Inventory],
        wp.[Resell - On Hand Inventory],
        wp.[1001 W Upland - On Hand Inventory],
        wp.[Nevada Marion - On Hand Inventory],
        wp.[NV FC Transfers - On Hand Inventory],
        wp.[Indiana FC - On Hand Inventory],
        wp.[Nevada Lamb - On Hand Inventory],
        wp.[Parts - On Hand Inventory],
        wp.[Nevada Offsite Storage - On Hand Inventory],
        wp.[Pennsylvania Overflow - On Hand Inventory],
        wp.[Nevada Ann - On Hand Inventory],
        wp.[Georgia - On Hand Inventory],
        wp.[Texas Wintergreen - On Hand Inventory],
        wp.[NE FC Transfers - On Hand Inventory],
        wp.[GA FC Transfers - On Hand Inventory],
        wp.[TX FC Transfers - On Hand Inventory],
        wp.[IN FC Transfers - On Hand Inventory],
        wp.[PA FC Transfers - On Hand Inventory],
        wp.[Indiana Greenwood - On Hand Inventory],
        wp.[Pennsylvania Nanticoke - On Hand Inventory],
        wp.[1421 Kingbird - Net Daily Adjustments],
        wp.[2610 West L - Net Daily Adjustments],
        wp.[4021 N 27th - Net Daily Adjustments],
        wp.[Nevada - Net Daily Adjustments],
        wp.[1000 Commerce - Net Daily Adjustments],
        wp.[Pennsylvania - Net Daily Adjustments],
        wp.[Resell - Net Daily Adjustments],
        wp.[1001 W Upland - Net Daily Adjustments],
        wp.[Nevada Marion - Net Daily Adjustments],
        wp.[NV FC Transfers - Net Daily Adjustments],
        wp.[Indiana FC - Net Daily Adjustments],
        wp.[Nevada Lamb - Net Daily Adjustments],
        wp.[Parts - Net Daily Adjustments],
        wp.[Nevada Offsite Storage - Net Daily Adjustments],
        wp.[Pennsylvania Overflow - Net Daily Adjustments],
        wp.[Nevada Ann - Net Daily Adjustments],
        wp.[Georgia - Net Daily Adjustments],
        wp.[Texas Wintergreen - Net Daily Adjustments],
        wp.[NE FC Transfers - Net Daily Adjustments],
        wp.[GA FC Transfers - Net Daily Adjustments],
        wp.[TX FC Transfers - Net Daily Adjustments],
        wp.[IN FC Transfers - Net Daily Adjustments],
        wp.[PA FC Transfers - Net Daily Adjustments],
        wp.[Indiana Greenwood - Net Daily Adjustments],
        wp.[Pennsylvania Nanticoke - Net Daily Adjustments],
        wp.[1421 Kingbird - OOS],
        wp.[2610 West L - OOS],
        wp.[4021 N 27th - OOS],
        wp.[Nevada - OOS],
        wp.[1000 Commerce - OOS],
        wp.[Pennsylvania - OOS],
        wp.[Resell - OOS],
        wp.[1001 W Upland - OOS],
        wp.[Nevada Marion - OOS],
        wp.[NV FC Transfers - OOS],
        wp.[Indiana FC - OOS],
        wp.[Nevada Lamb - OOS],
        wp.[Parts - OOS],
        wp.[Nevada Offsite Storage - OOS],
        wp.[Pennsylvania Overflow - OOS],
        wp.[Nevada Ann - OOS],
        wp.[Georgia - OOS],
        wp.[Texas Wintergreen - OOS],
        wp.[NE FC Transfers - OOS],
        wp.[GA FC Transfers - OOS],
        wp.[TX FC Transfers - OOS],
        wp.[IN FC Transfers - OOS],
        wp.[PA FC Transfers - OOS],
        wp.[Indiana Greenwood - OOS],
        wp.[Pennsylvania Nanticoke - OOS]
    FROM temp.SalesFactor sf
    LEFT OUTER JOIN temp.WarehousePivot wp ON wp.ItemID = sf.ItemID AND
                                          wp.Date = sf.Date
    LEFT OUTER JOIN temp.RollingSales rs ON rs.ItemID = sf.ItemID AND
                                        rs.Date = sf.Date
    LEFT OUTER JOIN temp.PivotProductWindow ppw ON ppw.ItemID = sf.ItemID AND
                                                   ppw.Date = sf.Date
    LEFT OUTER JOIN temp.ItemGradeCutoffs pig30 ON
        CASE
            WHEN sf.TotalS30 != 0 THEN CAST(sf.RollingTotalS30 AS DECIMAL(38, 18))/sf.TotalS30 ELSE 0
        END > pig30.PrevCutoff AND
        CASE
            WHEN sf.TotalS30 != 0 THEN CAST(sf.RollingTotalS30 AS DECIMAL(38, 18))/sf.TotalS30 ELSE 0
        END <= pig30.PercCutoff
    LEFT OUTER JOIN temp.ItemGradeCutoffs pig90 ON
        CASE
            WHEN sf.TotalS90 != 0 THEN CAST(sf.RollingTotalS90 AS DECIMAL(38, 18))/sf.TotalS90 ELSE 0
        END > pig90.PrevCutoff AND
        CASE
            WHEN sf.TotalS90 != 0 THEN CAST(sf.RollingTotalS90 AS DECIMAL(38, 18))/sf.TotalS90 ELSE 0
        END <= pig90.PercCutoff
    LEFT OUTER JOIN temp.ItemGradeCutoffs pig365 ON
        CASE
            WHEN sf.TotalS365 != 0 THEN CAST(sf.RollingTotalS365 AS DECIMAL(38, 18))/sf.TotalS365 ELSE 0
        END > pig365.PrevCutoff AND
        CASE
            WHEN sf.TotalS365 != 0 THEN CAST(sf.RollingTotalS365 AS DECIMAL(38, 18))/sf.TotalS365 ELSE 0
        END <= pig365.PercCutoff
    LEFT OUTER JOIN temp.ItemGradeCutoffs sig30 ON
        CASE
            WHEN sf.TotalS30 != 0 THEN
                CASE
                    WHEN (sf.RollingTotalS30*sf.ScaleFactor30)/sf.TotalS30 > 1 THEN 1
                    ELSE (sf.RollingTotalS30*sf.ScaleFactor30)/sf.TotalS30
                END
            ELSE 0
        END > sig30.PrevCutoff AND
        CASE
            WHEN sf.TotalS30 != 0 THEN
                CASE
                    WHEN (sf.RollingTotalS30*sf.ScaleFactor30)/sf.TotalS30 > 1 THEN 1
                    ELSE (sf.RollingTotalS30*sf.ScaleFactor30)/sf.TotalS30
                END
            ELSE 0
        END <= sig30.PercCutoff
    LEFT OUTER JOIN temp.ItemGradeCutoffs sig90 ON
        CASE
            WHEN sf.TotalS90 != 0 THEN
                CASE
                    WHEN (sf.RollingTotalS90*sf.ScaleFactor90)/sf.TotalS90 > 1 THEN 1
                    ELSE (sf.RollingTotalS90*sf.ScaleFactor90)/sf.TotalS90
                END
            ELSE 0
        END > sig90.PrevCutoff AND
        CASE
            WHEN sf.TotalS90 != 0 THEN
                CASE
                    WHEN (sf.RollingTotalS90*sf.ScaleFactor90)/sf.TotalS90 > 1 THEN 1
                    ELSE (sf.RollingTotalS90*sf.ScaleFactor90)/sf.TotalS90
                END
            ELSE 0
        END <= sig90.PercCutoff
    LEFT OUTER JOIN temp.ItemGradeCutoffs sig365 ON
        CASE
            WHEN sf.TotalS365 != 0 THEN
                CASE
                    WHEN (sf.RollingTotalS365*sf.ScaleFactor365)/sf.TotalS365 > 1 THEN 1
                    ELSE (sf.RollingTotalS365*sf.ScaleFactor365)/sf.TotalS365
                END
            ELSE 0
        END > sig365.PrevCutoff AND
        CASE
            WHEN sf.TotalS365 != 0 THEN
                CASE
                    WHEN (sf.RollingTotalS365*sf.ScaleFactor365)/sf.TotalS365 > 1 THEN 1
                    ELSE (sf.RollingTotalS365*sf.ScaleFactor365)/sf.TotalS365
                END
            ELSE 0
        END <= sig365.PercCutoff;

    CREATE CLUSTERED INDEX temp_PivotProduct ON temp.PivotProduct(ItemID, Date);

END
CREATE OR ALTER PROCEDURE ssis.usp_Update_temp_WarehousePivot

AS


BEGIN


    DROP TABLE IF EXISTS temp.WarehousePivot;
    CREATE TABLE temp.WarehousePivot (
            Date DATE,
            ItemID INT,
            [1421 Kingbird - On Hand Inventory] INT,
            [2610 West L - On Hand Inventory] INT,
            [4021 N 27th - On Hand Inventory] INT,
            [Nevada - On Hand Inventory] INT,
            [1000 Commerce - On Hand Inventory] INT,
            [Pennsylvania - On Hand Inventory] INT,
            [Resell - On Hand Inventory] INT,
            [1001 W Upland - On Hand Inventory] INT,
            [Nevada Marion - On Hand Inventory] INT,
            [NV FC Transfers - On Hand Inventory] INT,
            [Indiana FC - On Hand Inventory] INT,
            [Nevada Lamb - On Hand Inventory] INT,
            [Parts - On Hand Inventory] INT,
            [Nevada Offsite Storage - On Hand Inventory] INT,
            [Pennsylvania Overflow - On Hand Inventory] INT,
            [Nevada Ann - On Hand Inventory] INT,
            [Georgia - On Hand Inventory] INT,
            [Texas Wintergreen - On Hand Inventory] INT,
            [NE FC Transfers - On Hand Inventory] INT,
            [GA FC Transfers - On Hand Inventory] INT,
            [TX FC Transfers - On Hand Inventory] INT,
            [IN FC Transfers - On Hand Inventory] INT,
            [PA FC Transfers - On Hand Inventory] INT,
            [Indiana Greenwood - On Hand Inventory] INT,
            [Pennsylvania Nanticoke - On Hand Inventory] INT,
            [1421 Kingbird - Net Daily Adjustments] INT,
            [2610 West L - Net Daily Adjustments] INT,
            [4021 N 27th - Net Daily Adjustments] INT,
            [Nevada - Net Daily Adjustments] INT,
            [1000 Commerce - Net Daily Adjustments] INT,
            [Pennsylvania - Net Daily Adjustments] INT,
            [Resell - Net Daily Adjustments] INT,
            [1001 W Upland - Net Daily Adjustments] INT,
            [Nevada Marion - Net Daily Adjustments] INT,
            [NV FC Transfers - Net Daily Adjustments] INT,
            [Indiana FC - Net Daily Adjustments] INT,
            [Nevada Lamb - Net Daily Adjustments] INT,
            [Parts - Net Daily Adjustments] INT,
            [Nevada Offsite Storage - Net Daily Adjustments] INT,
            [Pennsylvania Overflow - Net Daily Adjustments] INT,
            [Nevada Ann - Net Daily Adjustments] INT,
            [Georgia - Net Daily Adjustments] INT,
            [Texas Wintergreen - Net Daily Adjustments] INT,
            [NE FC Transfers - Net Daily Adjustments] INT,
            [GA FC Transfers - Net Daily Adjustments] INT,
            [TX FC Transfers - Net Daily Adjustments] INT,
            [IN FC Transfers - Net Daily Adjustments] INT,
            [PA FC Transfers - Net Daily Adjustments] INT,
            [Indiana Greenwood - Net Daily Adjustments] INT,
            [Pennsylvania Nanticoke - Net Daily Adjustments] INT,
            [1421 Kingbird - OOS]BIT,
            [2610 West L - OOS]BIT,
            [4021 N 27th - OOS]BIT,
            [Nevada - OOS]BIT,
            [1000 Commerce - OOS]BIT,
            [Pennsylvania - OOS]BIT,
            [Resell - OOS]BIT,
            [1001 W Upland - OOS]BIT,
            [Nevada Marion - OOS]BIT,
            [NV FC Transfers - OOS]BIT,
            [Indiana FC - OOS]BIT,
            [Nevada Lamb - OOS]BIT,
            [Parts - OOS]BIT,
            [Nevada Offsite Storage - OOS]BIT,
            [Pennsylvania Overflow - OOS]BIT,
            [Nevada Ann - OOS]BIT,
            [Georgia - OOS]BIT,
            [Texas Wintergreen - OOS]BIT,
            [NE FC Transfers - OOS]BIT,
            [GA FC Transfers - OOS]BIT,
            [TX FC Transfers - OOS]BIT,
            [IN FC Transfers - OOS]BIT,
            [PA FC Transfers - OOS]BIT,
            [Indiana Greenwood - OOS]BIT,
            [Pennsylvania Nanticoke - OOS] BIT
    );

    WITH WarehousePivot AS (
        SELECT
            onhndPvt.Date,
            onhndPvt.ItemID,
            COALESCE([1421 Kingbird - On Hand Inventory], 0) [1421 Kingbird - On Hand Inventory],
            COALESCE([1421 Kingbird - Net Daily Adjustments], 0) [1421 Kingbird - Net Daily Adjustments],
            COALESCE([2610 West L - On Hand Inventory], 0) [2610 West L - On Hand Inventory],
            COALESCE([2610 West L - Net Daily Adjustments], 0) [2610 West L - Net Daily Adjustments],
            COALESCE([4021 N 27th - On Hand Inventory], 0) [4021 N 27th - On Hand Inventory],
            COALESCE([4021 N 27th - Net Daily Adjustments], 0) [4021 N 27th - Net Daily Adjustments],
            COALESCE([Nevada - On Hand Inventory], 0) [Nevada - On Hand Inventory],
            COALESCE([Nevada - Net Daily Adjustments], 0) [Nevada - Net Daily Adjustments],
            COALESCE([1000 Commerce - On Hand Inventory], 0) [1000 Commerce - On Hand Inventory],
            COALESCE([1000 Commerce - Net Daily Adjustments], 0) [1000 Commerce - Net Daily Adjustments],
            COALESCE([Pennsylvania - On Hand Inventory], 0) [Pennsylvania - On Hand Inventory],
            COALESCE([Pennsylvania - Net Daily Adjustments], 0) [Pennsylvania - Net Daily Adjustments],
            COALESCE([Resell - On Hand Inventory], 0) [Resell - On Hand Inventory],
            COALESCE([Resell - Net Daily Adjustments], 0) [Resell - Net Daily Adjustments],
            COALESCE([1001 W Upland - On Hand Inventory], 0) [1001 W Upland - On Hand Inventory],
            COALESCE([1001 W Upland - Net Daily Adjustments], 0) [1001 W Upland - Net Daily Adjustments],
            --COALESCE([Resell - On Hand Inventory], 0) [Resell - On Hand Inventory], --This is a separate Resell warehouse
            --COALESCE([Resell - Net Daily Adjustments], 0) [Resell - Net Daily Adjustments], --This is a separate Resell warehouse
            COALESCE([Nevada Marion - On Hand Inventory], 0) [Nevada Marion - On Hand Inventory],
            COALESCE([Nevada Marion - Net Daily Adjustments], 0) [Nevada Marion - Net Daily Adjustments],
            COALESCE([NV FC Transfers - On Hand Inventory], 0) [NV FC Transfers - On Hand Inventory],
            COALESCE([NV FC Transfers - Net Daily Adjustments], 0) [NV FC Transfers - Net Daily Adjustments],
            COALESCE([Indiana FC - On Hand Inventory], 0) [Indiana FC - On Hand Inventory],
            COALESCE([Indiana FC - Net Daily Adjustments], 0) [Indiana FC - Net Daily Adjustments],
            COALESCE([Nevada Lamb - On Hand Inventory], 0) [Nevada Lamb - On Hand Inventory],
            COALESCE([Nevada Lamb - Net Daily Adjustments], 0) [Nevada Lamb - Net Daily Adjustments],
            COALESCE([Parts - On Hand Inventory], 0) [Parts - On Hand Inventory],
            COALESCE([Parts - Net Daily Adjustments], 0) [Parts - Net Daily Adjustments],
            COALESCE([Nevada Offsite Storage - On Hand Inventory], 0) [Nevada Offsite Storage - On Hand Inventory],
            COALESCE([Nevada Offsite Storage - Net Daily Adjustments], 0) [Nevada Offsite Storage - Net Daily Adjustments],
            COALESCE([Pennsylvania Overflow - On Hand Inventory], 0) [Pennsylvania Overflow - On Hand Inventory],
            COALESCE([Pennsylvania Overflow - Net Daily Adjustments], 0) [Pennsylvania Overflow - Net Daily Adjustments],
            COALESCE([Nevada Ann - On Hand Inventory], 0) [Nevada Ann - On Hand Inventory],
            COALESCE([Nevada Ann - Net Daily Adjustments], 0) [Nevada Ann - Net Daily Adjustments],
            COALESCE([Georgia - On Hand Inventory], 0) [Georgia - On Hand Inventory],
            COALESCE([Georgia - Net Daily Adjustments], 0) [Georgia - Net Daily Adjustments],
            COALESCE([Texas Wintergreen - On Hand Inventory], 0) [Texas Wintergreen - On Hand Inventory],
            COALESCE([Texas Wintergreen - Net Daily Adjustments], 0) [Texas Wintergreen - Net Daily Adjustments],
            COALESCE([NE FC Transfers - On Hand Inventory], 0) [NE FC Transfers - On Hand Inventory],
            COALESCE([NE FC Transfers - Net Daily Adjustments], 0) [NE FC Transfers - Net Daily Adjustments],
            COALESCE([GA FC Transfers - On Hand Inventory], 0) [GA FC Transfers - On Hand Inventory],
            COALESCE([GA FC Transfers - Net Daily Adjustments], 0) [GA FC Transfers - Net Daily Adjustments],
            COALESCE([TX FC Transfers - On Hand Inventory], 0) [TX FC Transfers - On Hand Inventory],
            COALESCE([TX FC Transfers - Net Daily Adjustments], 0) [TX FC Transfers - Net Daily Adjustments],
            COALESCE([IN FC Transfers - On Hand Inventory], 0) [IN FC Transfers - On Hand Inventory],
            COALESCE([IN FC Transfers - Net Daily Adjustments], 0) [IN FC Transfers - Net Daily Adjustments],
            COALESCE([PA FC Transfers - On Hand Inventory], 0) [PA FC Transfers - On Hand Inventory],
            COALESCE([PA FC Transfers - Net Daily Adjustments], 0) [PA FC Transfers - Net Daily Adjustments],
            COALESCE([Indiana Greenwood - On Hand Inventory], 0) [Indiana Greenwood - On Hand Inventory],
            COALESCE([Indiana Greenwood - Net Daily Adjustments], 0) [Indiana Greenwood - Net Daily Adjustments],
            COALESCE([Pennsylvania Nanticoke - On Hand Inventory], 0) [Pennsylvania Nanticoke - On Hand Inventory],
            COALESCE([Pennsylvania Nanticoke - Net Daily Adjustments], 0) [Pennsylvania Nanticoke - Net Daily Adjustments]
        FROM (
            SELECT
                Date,
                ItemID,
                CONCAT(WarehouseName, ' - On Hand Inventory') onhndName,
                OnHandFCInventory
            FROM temp.DateExplodeProduct dep
            LEFT OUTER JOIN lookup.Warehouse w ON w.WarehouseID = dep.WarehouseID
        ) onhndVals
        PIVOT(MAX(OnHandFCInventory)FOR onhndName IN (
            [1421 Kingbird - On Hand Inventory],
            [2610 West L - On Hand Inventory],
            [4021 N 27th - On Hand Inventory],
            [Nevada - On Hand Inventory],
            [1000 Commerce - On Hand Inventory],
            [Pennsylvania - On Hand Inventory],
            [Resell - On Hand Inventory],
            [1001 W Upland - On Hand Inventory],
            [Nevada Marion - On Hand Inventory],
            [NV FC Transfers - On Hand Inventory],
            [Indiana FC - On Hand Inventory],
            [Nevada Lamb - On Hand Inventory],
            [Parts - On Hand Inventory],
            [Nevada Offsite Storage - On Hand Inventory],
            [Pennsylvania Overflow - On Hand Inventory],
            [Nevada Ann - On Hand Inventory],
            [Georgia - On Hand Inventory],
            [Texas Wintergreen - On Hand Inventory],
            [NE FC Transfers - On Hand Inventory],
            [GA FC Transfers - On Hand Inventory],
            [TX FC Transfers - On Hand Inventory],
            [IN FC Transfers - On Hand Inventory],
            [PA FC Transfers - On Hand Inventory],
            [Indiana Greenwood - On Hand Inventory],
            [Pennsylvania Nanticoke - On Hand Inventory]
        )) onhndPvt
        LEFT OUTER JOIN (
            SELECT
                Date,
                ItemID,
                CONCAT(WarehouseName, ' - Net Daily Adjustments') adjName,
                NetDailyAdjustmentQty
            FROM temp.DateExplodeProduct dep
            LEFT OUTER JOIN lookup.Warehouse w ON w.WarehouseID = dep.WarehouseID
        ) adjVals
        PIVOT(MAX(NetDailyAdjustmentQty)FOR adjName IN (
            [1421 Kingbird - Net Daily Adjustments],
            [2610 West L - Net Daily Adjustments],
            [4021 N 27th - Net Daily Adjustments],
            [Nevada - Net Daily Adjustments],
            [1000 Commerce - Net Daily Adjustments],
            [Pennsylvania - Net Daily Adjustments],
            [1001 W Upland - Net Daily Adjustments],
            [Resell - Net Daily Adjustments],
            [Nevada Marion - Net Daily Adjustments],
            [NV FC Transfers - Net Daily Adjustments],
            [Indiana FC - Net Daily Adjustments],
            [Nevada Lamb - Net Daily Adjustments],
            [Parts - Net Daily Adjustments],
            [Nevada Offsite Storage - Net Daily Adjustments],
            [Pennsylvania Overflow - Net Daily Adjustments],
            [Nevada Ann - Net Daily Adjustments],
            [Georgia - Net Daily Adjustments],
            [Texas Wintergreen - Net Daily Adjustments],
            [NE FC Transfers - Net Daily Adjustments],
            [GA FC Transfers - Net Daily Adjustments],
            [TX FC Transfers - Net Daily Adjustments],
            [IN FC Transfers - Net Daily Adjustments],
            [PA FC Transfers - Net Daily Adjustments],
            [Indiana Greenwood - Net Daily Adjustments],
            [Pennsylvania Nanticoke - Net Daily Adjustments]
        )) adjPvt ON adjPvt.ItemID = onhndPvt.ItemID AND
            adjPvt.Date = onhndPvt.Date
    )
    INSERT INTO temp.WarehousePivot
    SELECT
        Date,
        ItemID,
        [1421 Kingbird - On Hand Inventory],
        [2610 West L - On Hand Inventory],
        [4021 N 27th - On Hand Inventory],
        [Nevada - On Hand Inventory],
        [1000 Commerce - On Hand Inventory],
        [Pennsylvania - On Hand Inventory],
        [Resell - On Hand Inventory],
        [1001 W Upland - On Hand Inventory],
        [Nevada Marion - On Hand Inventory],
        [NV FC Transfers - On Hand Inventory],
        [Indiana FC - On Hand Inventory],
        [Nevada Lamb - On Hand Inventory],
        [Parts - On Hand Inventory],
        [Nevada Offsite Storage - On Hand Inventory],
        [Pennsylvania Overflow - On Hand Inventory],
        [Nevada Ann - On Hand Inventory],
        [Georgia - On Hand Inventory],
        [Texas Wintergreen - On Hand Inventory],
        [NE FC Transfers - On Hand Inventory],
        [GA FC Transfers - On Hand Inventory],
        [TX FC Transfers - On Hand Inventory],
        [IN FC Transfers - On Hand Inventory],
        [PA FC Transfers - On Hand Inventory],
        [Indiana Greenwood - On Hand Inventory],
        [Pennsylvania Nanticoke - On Hand Inventory],
        [1421 Kingbird - Net Daily Adjustments],
        [2610 West L - Net Daily Adjustments],
        [4021 N 27th - Net Daily Adjustments],
        [Nevada - Net Daily Adjustments],
        [1000 Commerce - Net Daily Adjustments],
        [Pennsylvania - Net Daily Adjustments],
        [Resell - Net Daily Adjustments],
        [1001 W Upland - Net Daily Adjustments],
        [Nevada Marion - Net Daily Adjustments],
        [NV FC Transfers - Net Daily Adjustments],
        [Indiana FC - Net Daily Adjustments],
        [Nevada Lamb - Net Daily Adjustments],
        [Parts - Net Daily Adjustments],
        [Nevada Offsite Storage - Net Daily Adjustments],
        [Pennsylvania Overflow - Net Daily Adjustments],
        [Nevada Ann - Net Daily Adjustments],
        [Georgia - Net Daily Adjustments],
        [Texas Wintergreen - Net Daily Adjustments],
        [NE FC Transfers - Net Daily Adjustments],
        [GA FC Transfers - Net Daily Adjustments],
        [TX FC Transfers - Net Daily Adjustments],
        [IN FC Transfers - Net Daily Adjustments],
        [PA FC Transfers - Net Daily Adjustments],
        [Indiana Greenwood - Net Daily Adjustments],
        [Pennsylvania Nanticoke - Net Daily Adjustments],
        CAST(CASE WHEN [1421 Kingbird - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[1421 Kingbird - OOS],
        CAST(CASE WHEN [2610 West L - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[2610 West L - OOS],
        CAST(CASE WHEN [4021 N 27th - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[4021 N 27th - OOS],
        CAST(CASE WHEN [Nevada - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[Nevada - OOS],
        CAST(CASE WHEN [1000 Commerce - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[1000 Commerce - OOS],
        CAST(CASE WHEN [Pennsylvania - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[Pennsylvania - OOS],
        CAST(CASE WHEN [Resell - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[Resell - OOS],
        CAST(CASE WHEN [1001 W Upland - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[1001 W Upland - OOS],
        CAST(CASE WHEN [Nevada Marion - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[Nevada Marion - OOS],
        CAST(CASE WHEN [NV FC Transfers - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[NV FC Transfers - OOS],
        CAST(CASE WHEN [Indiana FC - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[Indiana FC - OOS],
        CAST(CASE WHEN [Nevada Lamb - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[Nevada Lamb - OOS],
        CAST(CASE WHEN [Parts - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[Parts - OOS],
        CAST(CASE WHEN [Nevada Offsite Storage - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[Nevada Offsite Storage - OOS],
        CAST(CASE WHEN [Pennsylvania Overflow - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[Pennsylvania Overflow - OOS],
        CAST(CASE WHEN [Nevada Ann - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[Nevada Ann - OOS],
        CAST(CASE WHEN [Georgia - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[Georgia - OOS],
        CAST(CASE WHEN [Texas Wintergreen - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[Texas Wintergreen - OOS],
        CAST(CASE WHEN [NE FC Transfers - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[NE FC Transfers - OOS],
        CAST(CASE WHEN [GA FC Transfers - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[GA FC Transfers - OOS],
        CAST(CASE WHEN [TX FC Transfers - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[TX FC Transfers - OOS],
        CAST(CASE WHEN [IN FC Transfers - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[IN FC Transfers - OOS],
        CAST(CASE WHEN [PA FC Transfers - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[PA FC Transfers - OOS],
        CAST(CASE WHEN [Indiana Greenwood - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[Indiana Greenwood - OOS],
        CAST(CASE WHEN [Pennsylvania Nanticoke - On Hand Inventory] <= 0 THEN 1 ELSE 0 END AS BIT)[Pennsylvania Nanticoke - OOS]
    FROM WarehousePivot;

    CREATE CLUSTERED INDEX temp_WarehousePivot ON temp.WarehousePivot(ItemID, Date);
    CREATE NONCLUSTERED INDEX temp_WarehousePivot_Date ON temp.WarehousePivot(Date);


END
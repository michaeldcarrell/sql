CREATE OR ALTER PROCEDURE temp.usp_Update_temp_UnpivotedProduct

AS

BEGIN
    DROP TABLE IF EXISTS temp.UnpivotedProduct;
    CREATE TABLE temp.UnpivotedProduct(
        AdjustmentDate INT,
        NextAdjustmentDate INT,
        ItemID INT,
        WarehouseID INT,
        NetAdjustmentQty INT,
        OnHandFCInventory INT,
        LastSaleDate DATE,
        SaleEligibleOOS BIT,
    );


    WITH PreviousSaleDate AS (
        SELECT
            LAG(CAST(AdjustmentDate AS DATE), 1, CAST(AdjustmentDate AS DATE))
                OVER(PARTITION BY lsi.ItemID ORDER BY CAST(AdjustmentDate AS DATE)) LastSaleDate,
            CAST(AdjustmentDate AS DATE) AdjustmentDate,
            lsi.ItemID
        FROM stage.LSInventory lsi
        JOIN temp.DateLimiter dl ON dl.Date = lsi.AdjustmentDt
        JOIN temp.ItemLimiter limit ON lsi.ItemID = limit.ItemID
        WHERE AdjustmentReasonID IN (17, 20)
        GROUP BY lsi.ItemID, CAST(AdjustmentDate AS DATE)
    )
    INSERT INTO temp.UnpivotedProduct
    SELECT
        CAST(FORMAT(pip.AdjustmentDate, 'yyyyMMdd') AS INT) AdjustmentDate,
        CAST(FORMAT(LEAD(pip.AdjustmentDate, 1, CAST(GETDATE() AS DATE))
            OVER(PARTITION BY pip.ItemID, pip.WarehouseID ORDER BY pip.AdjustmentDate), 'yyyyMMdd') AS INT) NextAdjustmentDate,
        pip.ItemID,
        pip.WarehouseID,
        pip.NetAdjustmentQty,
        SUM(pip.NetAdjustmentQty) OVER (PARTITION BY pip.ItemID, WarehouseID ORDER BY pip.AdjustmentDate) OnHandFCInventory,
        psd.LastSaleDate, --Store this so we don't have to calc this in historicals
        CAST(CASE
            WHEN DATEDIFF(DAY, LastSaleDate, pip.AdjustmentDate) <= 365 THEN 1
            ELSE 0
        END AS BIT) SaleEligibleOOS
    FROM temp.ProductInitialPull pip
    JOIN PreviousSaleDate psd ON psd.ItemID = pip.ItemID AND
                                            pip.AdjustmentDate >= psd.LastSaleDate AND
                                            pip.AdjustmentDate < psd.AdjustmentDate

    CREATE NONCLUSTERED INDEX temp_UnpivotedProduct_Date ON temp.UnpivotedProduct(AdjustmentDate) INCLUDE(NextAdjustmentDate);
    CREATE CLUSTERED INDEX temp_UnpivotedProduct ON temp.UnpivotedProduct(ItemID, AdjustmentDate, NextAdjustmentDate);
END

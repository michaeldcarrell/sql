CREATE OR ALTER PROCEDURE ssis.usp_Update_temp_SalesFactor

AS

BEGIN

    DROP TABLE IF EXISTS temp.SalesFactor;
    CREATE TABLE temp.SalesFactor (
                Date DATE,
                ItemID INT,
                OOSEligible BIT,
                PureRankS30 INT,
                PureRankS90 INT,
                PureRankS365 INT,
                RollingTotalS30 INT,
                RollingTotalS90 INT,
                RollingTotalS365 INT,
                TotalS30 INT,
                TotalS90 INT,
                TotalS365 INT,
                ScaleFactor365 DECIMAL(38, 18),
                ScaleFactor90 DECIMAL(38, 18),
                ScaleFactor30 DECIMAL(38, 18)
    );

    INSERT INTO temp.SalesFactor
    SELECT
            Date,
            ItemID,
            CASE
                WHEN (SaleOOSEligible = 1 OR DaysSinceRCVOOSEligible = 1) AND InventoryStatusOOSEligible = 1 THEN 1 ELSE 0
            END OOSEligible,
            RANK() OVER(PARTITION BY Date ORDER BY S30 DESC) PureRankS30,
            RANK() OVER(PARTITION BY Date ORDER BY S90 DESC) PureRankS90,
            RANK() OVER(PARTITION BY Date ORDER BY S365 DESC) PureRankS365,
            CAST(SUM(S30) OVER(PARTITION BY Date ORDER BY S30 DESC) AS DECIMAL(38, 18)) RollingTotalS30,
            CAST(SUM(S90) OVER(PARTITION BY Date ORDER BY S90 DESC) AS DECIMAL(38, 18)) RollingTotalS90,
            CAST(SUM(S365) OVER(PARTITION BY Date ORDER BY S365 DESC) AS DECIMAL(38, 18)) RollingTotalS365,
            CAST(SUM(S30) OVER(PARTITION BY Date ORDER BY Date) AS DECIMAL(38, 18)) TotalS30,
            CAST(SUM(S90) OVER(PARTITION BY Date ORDER BY Date) AS DECIMAL(38, 18)) TotalS90,
            CAST(SUM(S365) OVER(PARTITION BY Date ORDER BY Date) AS DECIMAL(38, 18)) TotalS365,
            ISNULL(
                CAST(1 AS DECIMAL(38,18))
                    /NULLIF(
                        (1-(CAST(SUM(CAST([Network - OOS] AS INT)) OVER (PARTITION BY ItemID ORDER BY Date ROWS BETWEEN 364 PRECEDING AND CURRENT ROW) AS DECIMAL(38,18))
                            /CAST(365 AS DECIMAL(38,18))))
                                ,0)
                                    ,1) AS ScaleFactor365,
            ISNULL(
                CAST(1 AS DECIMAL(38,18))
                    /NULLIF(
                        (1-(CAST(SUM(CAST([Network - OOS] AS INT)) OVER (PARTITION BY ItemID ORDER BY Date ROWS BETWEEN 89 PRECEDING AND CURRENT ROW) AS DECIMAL(38,18))
                            /CAST(90 AS DECIMAL(38,18))))
                                ,0)
                                    ,1) AS ScaleFactor90,
            ISNULL(
                CAST(1 AS DECIMAL(38,18))
                    /NULLIF(
                        (1-(CAST(SUM(CAST([Network - OOS] AS INT)) OVER (PARTITION BY ItemID ORDER BY Date ROWS BETWEEN 29 PRECEDING AND CURRENT ROW) AS DECIMAL(38,18))
                            /CAST(90 AS DECIMAL(38,18))))
                                ,0)
                                    ,1) AS ScaleFactor30
    FROM temp.RollingSales;

    CREATE NONCLUSTERED INDEX temp_SalesFactor_Date ON temp.SalesFactor(Date);
    CREATE CLUSTERED COLUMNSTORE INDEX temp_SalesFactor ON temp.SalesFactor

END


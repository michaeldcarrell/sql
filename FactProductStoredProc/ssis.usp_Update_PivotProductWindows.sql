CREATE OR ALTER PROCEDURE ssis.usp_Update_PivotProductWindow

AS

BEGIN

    DROP TABLE IF EXISTS temp.PivotProductWindow;
    CREATE TABLE temp.PivotProductWindow (
        Date DATE,
        ItemID INT,
        ScaledRankS30 INT,
        ScaledRankS90 INT,
        ScaledRankS365 INT,
        PureTop200SKUS30 BIT,
        PureTop200SKUS90 BIT,
        PureTop200SKUS365 BIT,
        ScaledTop200SKUS30 BIT,
        ScaledTop200SKUS90 BIT,
        ScaledTop200SKUS365 BIT
    );

    INSERT INTO temp.PivotProductWindow
    SELECT
        sf.Date,
        sf.ItemID,
        RANK() OVER(PARTITION BY sf.Date ORDER BY (rs.S30 * sf.ScaleFactor30) DESC) ScaledRankS30,
        RANK() OVER(PARTITION BY sf.Date ORDER BY (rs.S90 * sf.ScaleFactor90) DESC) ScaledRankS90,
        RANK() OVER(PARTITION BY sf.Date ORDER BY (rs.S365 * sf.ScaleFactor365) DESC) ScaledRankS365,
        CAST(CASE WHEN sf.PureRankS30 <= 200 THEN 1 ELSE 0 END AS BIT) PureTop200SKUS30,
        CAST(CASE WHEN sf.PureRankS90 <= 200 THEN 1 ELSE 0 END AS BIT) PureTop200SKUS90,
        CAST(CASE WHEN sf.PureRankS365 <= 200 THEN 1 ELSE 0 END AS BIT) PureTop200SKUS365,
        CAST(CASE WHEN RANK() OVER(PARTITION BY sf.Date ORDER BY (rs.S30 * sf.ScaleFactor30) DESC) <= 200 THEN 1 ELSE 0 END AS BIT) ScaledTop200SKUS30,
        CAST(CASE WHEN RANK() OVER(PARTITION BY sf.Date ORDER BY (rs.S90 * sf.ScaleFactor90) DESC) <= 200 THEN 1 ELSE 0 END AS BIT) ScaledTop200SKUS90,
        CAST(CASE WHEN RANK() OVER(PARTITION BY sf.Date ORDER BY (rs.S365 * sf.ScaleFactor365) DESC) <= 200 THEN 1 ELSE 0 END AS BIT) ScaledTop200SKUS365
    FROM temp.SalesFactor sf
    LEFT OUTER JOIN temp.RollingSales rs ON rs.ItemID = sf.ItemID AND
                                        rs.Date = sf.Date

    CREATE NONCLUSTERED INDEX temp_PivotProductWindow_Date ON temp.PivotProductWindow(Date)
    CREATE CLUSTERED INDEX temp_PivotProductWindow ON temp.PivotProductWindow(ItemID, Date)

END
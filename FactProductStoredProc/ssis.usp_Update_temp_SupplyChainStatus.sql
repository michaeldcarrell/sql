CREATE OR ALTER PROCEDURE ssis.usp_Update_temp_SupplyChainStatus

AS

BEGIN

    DROP TABLE IF EXISTS temp.SupplyChainStatus;
    CREATE TABLE temp.SupplyChainStatus(
        Date DATE,
        ItemID INT,
        ItemDateChanged DATE,
        ItemStatusID INT,
        InvDateChanged DATE,
        InventoryStatusID INT,
        ProcDateChanged DATE,
        ProcurementStatusID INT,
        SupplyChainStatusNodeID NVARCHAR(16)
    );


    WITH MaxStatusIDOfDay AS (
        SELECT
            isc.ItemID,
            CAST(DateChanged AS DATE) DateChanged,
            MAX(StatusChangeID) MaxStatusIDOfDay
        FROM stage.items_InventoryStatusChanges isc
        JOIN temp.ItemLimiter limit ON limit.ItemID = isc.ItemID
        GROUP BY isc.ItemID, CAST(DateChanged AS DATE)
    )/*, ItemFirstRCVDate AS (
        SELECT
            lsi.ItemID,
            MIN(CAST(AdjustmentDate AS DATE)) minAdjustmentDate
        FROM stage.LSInventory lsi
        JOIN temp.ItemLimiter limit ON limit.ItemID = lsi.ItemID
        GROUP BY lsi.ItemID
    )*/, FirstChangeRecord AS (
        SELECT
            MIN(StatusChangeID) minStatusChangedID,
            isc.ItemID
        FROM stage.items_InventoryStatusChanges isc
        JOIN temp.ItemLimiter limit ON limit.ItemID = isc.ItemID
        JOIN MaxStatusIDOfDay msiod ON msiod.MaxStatusIDOfDay = isc.StatusChangeID
        GROUP BY isc.ItemID
    ), LastChangeRecord AS (
        SELECT
            MAX(StatusChangeID) maxStatusChangedID,
            MAX(isc.DateChanged) maxChangedDate,
            isc.ItemID
        FROM stage.items_InventoryStatusChanges isc
        JOIN temp.ItemLimiter limit ON limit.ItemID = isc.ItemID
        JOIN MaxStatusIDOfDay msiod ON msiod.MaxStatusIDOfDay = isc.StatusChangeID
        GROUP BY isc.ItemID
    ), StatusChangeDates AS (
        SELECT
            CAST(limit.minAdjustmentDate AS DATE) DateChanged,
            CAST(DateChanged AS DATE) NextChangedDate,
            isc.ItemID,
            OldStatusID StatusID
        FROM stage.items_InventoryStatusChanges isc
        JOIN temp.ItemLimiter limit ON limit.ItemID = isc.ItemID
        JOIN FirstChangeRecord fcr ON fcr.minStatusChangedID = isc.StatusChangeID
        UNION ALL
        SELECT
            msiod.DateChanged,
            LEAD(msiod.DateChanged, 1, lcr.maxChangedDate) OVER (PARTITION BY isc.ItemID ORDER BY msiod.DateChanged) NextChangedDate,
            isc.ItemID,
            NewStatusID StatusID
        FROM stage.items_InventoryStatusChanges isc
        JOIN temp.ItemLimiter limit ON limit.ItemID = isc.ItemID
        JOIN MaxStatusIDOfDay msiod ON msiod.MaxStatusIDOfDay = isc.StatusChangeID
        LEFT OUTER JOIN LastChangeRecord lcr ON lcr.ItemID = isc.ItemID
        UNION ALL
        SELECT
            CAST(DateChanged AS DATE) NextChangedDate,
            CAST(GETDATE() + 1 AS DATE) DateChanged,
            isc.ItemID,
            NewStatusID StatusID
        FROM stage.items_InventoryStatusChanges isc
        JOIN temp.ItemLimiter limit ON limit.ItemID = isc.ItemID
        JOIN LastChangeRecord lcr ON lcr.maxStatusChangedID = isc.StatusChangeID
    ), ProcMaxStatusIDOfDay AS (
        SELECT
            psc.ItemID,
            CAST(DateChanged AS DATE) DateChanged,
            MAX(StatusChangeID) MaxStatusIDOfDay
        FROM stage.items_ProcurementStatusChanges psc
        JOIN temp.ItemLimiter limit ON limit.ItemID = psc.ItemID
        GROUP BY psc.ItemID, CAST(DateChanged AS DATE)
    ), ProcFirstChangeRecord AS (
        SELECT
            MIN(StatusChangeID) minStatusChangedID,
            psc.ItemID
        FROM stage.items_ProcurementStatusChanges psc
        JOIN temp.ItemLimiter limit ON limit.ItemID = psc.ItemID
        JOIN ProcMaxStatusIDOfDay msiod ON msiod.MaxStatusIDOfDay = psc.StatusChangeID
        GROUP BY psc.ItemID
    ), ProcLastChangeRecord AS (
        SELECT
            MAX(StatusChangeID) maxStatusChangedID,
            MAX(psc.DateChanged) maxChangedDate,
            psc.ItemID
        FROM stage.items_ProcurementStatusChanges psc
        JOIN temp.ItemLimiter limit ON limit.ItemID = psc.ItemID
        JOIN ProcMaxStatusIDOfDay msiod ON msiod.MaxStatusIDOfDay = psc.StatusChangeID
        GROUP BY psc.ItemID
    ), ProcStatusChangeDates AS (
        SELECT
            CAST(limit.minAdjustmentDate AS DATE) DateChanged,
            CAST(DateChanged AS DATE) NextChangedDate,
            psc.ItemID,
            OldStatusID StatusID
        FROM stage.items_ProcurementStatusChanges psc
        JOIN temp.ItemLimiter limit ON limit.ItemID = psc.ItemID
        JOIN ProcFirstChangeRecord fcr ON fcr.minStatusChangedID = psc.StatusChangeID
        UNION ALL
        SELECT
            msiod.DateChanged,
            LEAD(msiod.DateChanged, 1, lcr.maxChangedDate) OVER (PARTITION BY psc.ItemID ORDER BY msiod.DateChanged) NextChangedDate,
            psc.ItemID,
            NewStatusID StatusID
        FROM stage.items_ProcurementStatusChanges psc
        JOIN temp.ItemLimiter limit ON limit.ItemID = psc.ItemID
        JOIN ProcMaxStatusIDOfDay msiod ON msiod.MaxStatusIDOfDay = psc.StatusChangeID
        LEFT OUTER JOIN ProcLastChangeRecord lcr ON lcr.ItemID = psc.ItemID
        UNION ALL
        SELECT
            CAST(DateChanged AS DATE) NextChangedDate,
            CAST(GETDATE() + 1 AS DATE) DateChanged,
            psc.ItemID,
            NewStatusID StatusID
        FROM stage.items_ProcurementStatusChanges psc
        JOIN temp.ItemLimiter limit ON limit.ItemID = psc.ItemID
        JOIN ProcLastChangeRecord lcr ON lcr.maxStatusChangedID = psc.StatusChangeID
    ), ItemMaxStatusIDOfDay AS (
        SELECT
            itemsc.ItemID,
            CAST(DateChanged AS DATE) DateChanged,
            MAX(ChangeID) MaxStatusIDOfDay
        FROM stage.items_ItemStatusChanges itemsc
        JOIN temp.ItemLimiter limit ON limit.ItemID = itemsc.ItemID
        GROUP BY itemsc.ItemID, CAST(DateChanged AS DATE)
    ), ItemFirstChangeRecord AS (
        SELECT
            MIN(ChangeID) minStatusChangedID,
            itemsc.ItemID
        FROM stage.items_ItemStatusChanges itemsc
        JOIN temp.ItemLimiter limit ON limit.ItemID = itemsc.ItemID
        JOIN ItemMaxStatusIDOfDay msiod ON msiod.MaxStatusIDOfDay = itemsc.ChangeID
        GROUP BY itemsc.ItemID
    ), ItemLastChangeRecord AS (
        SELECT
            MAX(ChangeID) maxStatusChangedID,
            MAX(itemsc.DateChanged) maxChangedDate,
            itemsc.ItemID
        FROM stage.items_ItemStatusChanges itemsc
        JOIN temp.ItemLimiter limit ON limit.ItemID = itemsc.ItemID
        JOIN ItemMaxStatusIDOfDay msiod ON msiod.MaxStatusIDOfDay = itemsc.ChangeID
        GROUP BY itemsc.ItemID
    ), ItemStatusChangeDates AS (
        SELECT
            CAST(limit.minAdjustmentDate AS DATE) DateChanged,
            CAST(DateChanged AS DATE) NextChangedDate,
            itemsc.ItemID,
            OldStatusID StatusID
        FROM stage.items_ItemStatusChanges itemsc
        JOIN temp.ItemLimiter limit ON limit.ItemID = itemsc.ItemID
        JOIN ItemFirstChangeRecord fcr ON fcr.minStatusChangedID = itemsc.ChangeID
        UNION ALL
        SELECT
            msiod.DateChanged,
            LEAD(msiod.DateChanged, 1, lcr.maxChangedDate) OVER (PARTITION BY itemsc.ItemID ORDER BY msiod.DateChanged) NextChangedDate,
            itemsc.ItemID,
            NewStatusID StatusID
        FROM stage.items_ItemStatusChanges itemsc
        JOIN temp.ItemLimiter limit ON limit.ItemID = itemsc.ItemID
        JOIN ItemMaxStatusIDOfDay msiod ON msiod.MaxStatusIDOfDay = itemsc.ChangeID
        LEFT OUTER JOIN ItemLastChangeRecord lcr ON lcr.ItemID = itemsc.ItemID
        UNION ALL
        SELECT
            CAST(DateChanged AS DATE) NextChangedDate,
            CAST(GETDATE() + 1 AS DATE) DateChanged,
            itemsc.ItemID,
            NewStatusID StatusID
        FROM stage.items_ItemStatusChanges itemsc
        JOIN temp.ItemLimiter limit ON limit.ItemID = itemsc.ItemID
        JOIN ItemLastChangeRecord lcr ON lcr.maxStatusChangedID = itemsc.ChangeID
    )
    INSERT INTO temp.SupplyChainStatus
    SELECT
        d.Date,
        d.ItemID,
        iscd.DateChanged ItemDateChanged,
        iscd.StatusID ItemStatusID,
        scd.DateChanged InvDateChanged,
        scd.StatusID InventoryStatusID,
        pscd.DateChanged ProcDateChanged,
        pscd.StatusID ProcurementStatusID,
        CONCAT(
            COALESCE(iscd.StatusID, -1), '::',
            COALESCE(scd.StatusID, -1), '::',
            COALESCE(pscd.StatusID, -1)
        ) SupplyChainStatusNodeID
    FROM temp.SupplyChainStatusBase d
    LEFT OUTER JOIN ItemStatusChangeDates iscd ON d.Date >= iscd.DateChanged AND
                                                  d.Date < iscd.NextChangedDate AND
                                                  d.ItemID = iscd.ItemID
    LEFT OUTER JOIN StatusChangeDates scd ON d.Date >= scd.DateChanged AND
                                             d.Date < scd.NextChangedDate AND
                                             d.ItemID = scd.ItemID
    LEFT OUTER JOIN ProcStatusChangeDates pscd ON d.Date >= pscd.DateChanged AND
                                                  d.Date < pscd.NextChangedDate AND
                                                  d.ItemID = pscd.ItemID

    CREATE NONCLUSTERED INDEX temp_SupplyChainStatus_Date ON temp.SupplyChainStatus(Date);
    CREATE CLUSTERED INDEX temp_SupplyChainStatus ON temp.SupplyChainStatus(ItemID, Date);

END



CREATE OR ALTER PROCEDURE temp.usp_Update_temp_DateExplodeProduct

AS

BEGIN
    DROP TABLE IF EXISTS temp.DateExplodeProduct;
    CREATE TABLE temp.DateExplodeProduct(
        Date DATE,
        ItemID INT,
        WarehouseID INT,
        NetDailyAdjustmentQty INT,
        OnHandFCInventory INT,
        OnHandNetworkInventory INT,
        SaleEligibleOOS BIT,
        RCVEligibleOOS BIT,
    );


    INSERT INTO temp.DateExplodeProduct
    SELECT
        CAST(CAST(DateID AS NVARCHAR) AS DATE) Date,
        up.ItemID,
        up.WarehouseID,
        COALESCE(iday.NetAdjustmentQty, 0) NetDailyAdjustmentQty,
        up.OnHandFCInventory,
        SUM(up.OnHandFCInventory) OVER(PARTITION BY up.ItemID, DateID ORDER BY DateID) OnHandNetworkInventory,
        up.SaleEligibleOOS,
        CAST(CASE
            WHEN DATEDIFF(DAY, mrd.minAdjustmentDate, CAST(CAST(DateID AS NVARCHAR) AS DATE)) <= 365 THEN 1
            ELSE 0
        END AS BIT) RCVEligibleOOS
    FROM lookup.Date
    JOIN temp.UnpivotedProduct up ON Date.DateID >= up.AdjustmentDate AND
                                     Date.DateID < up.NextAdjustmentDate
    LEFT OUTER JOIN temp.UnpivotedProduct iday ON date.DateID = iday.AdjustmentDate AND
                                                    up.ItemID = iday.ItemID AND
                                                    up.WarehouseID = iday.WarehouseID
    LEFT OUTER JOIN temp.ItemLimiter mrd ON mrd.ItemID = up.ItemID

    CREATE NONCLUSTERED INDEX temp_DateExplodeProduct_Date ON temp.DateExplodeProduct(Date)
    CREATE CLUSTERED INDEX temp_DateExplode ON temp.DateExplodeProduct(ItemID, Date)

END

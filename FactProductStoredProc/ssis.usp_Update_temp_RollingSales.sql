CREATE OR ALTER PROCEDURE ssis.usp_Update_temp_RollingSales

AS

BEGIN

    DROP TABLE IF EXISTS temp.RollingSales;
    CREATE TABLE temp.RollingSales (
        Date DATE,
        SaleAdjustmentDate DATE,
        ItemID INT,
        SupplyChainStatusNodeID NVARCHAR(16),
        ItemStatus INT,
        InvStatus INT,
        ProcurementStatus INT,
        SaleOOSEligible BIT,
        DaysSinceRCVOOSEligible BIT,
        InventoryStatusOOSEligible BIT,
        [Network - Daily Unit Sales] INT,
        S365 INT,
        S90 INT,
        S30 INT,
        [Network - OOS] BIT
    );

    WITH SaleDates AS (
        SELECT
            CAST(AdjustmentDate AS DATE) SaleAdjustmentDate,
            LEAD(CAST(AdjustmentDate AS DATE), 1, CAST(GETDATE() AS DATE))
                OVER (PARTITION BY lsi.ItemID ORDER BY CAST(AdjustmentDate AS DATE)) NextSaleDate,
            lsi.ItemID
        FROM stage.LSInventory lsi
        WHERE AdjustmentReasonID IN (17, 20)
        GROUP BY lsi.ItemID, CAST(AdjustmentDate AS DATE)
    ), TempRoll AS (
        SELECT
            woos.Date,
            sd.SaleAdjustmentDate,
            woos.ItemID,
            scs.SupplyChainStatusNodeID,
            scs.ItemStatusID,
            scs.InventoryStatusID,
            scs.ProcurementStatusID,
            CAST(CASE
                WHEN DATEDIFF(DAY, sd.SaleAdjustmentDate, woos.Date) < 365 THEN 1 ELSE 0
            END AS BIT) SaleOOSEligible,
            CAST(CASE
                WHEN DATEDIFF(DAY, minAdjust.minAdjustmentDate, woos.Date) <= 365 THEN 1 ELSE 0
            END AS BIT) DaysSinceRCVOOSEligible,
            CAST(CASE
                WHEN scs.InventoryStatusID IN (0, 8, 13, 14, 19, 22, 24, 25) THEN 1 ELSE 0
            END AS BIT) InventoryStatusOOSEligible,
            COALESCE(pipa.SaleAdjustments, 0) [Network - Daily Unit Sales],
            SUM(COALESCE(pipa.SaleAdjustments, 0)) OVER(PARTITION BY woos.ItemID ORDER BY woos.Date
                ROWS BETWEEN 364 PRECEDING AND CURRENT ROW) AS S365Temp,
            SUM(COALESCE(pipa.SaleAdjustments, 0)) OVER(PARTITION BY woos.ItemID ORDER BY woos.Date
                ROWS BETWEEN 89 PRECEDING AND CURRENT ROW) AS S90Temp,
            SUM(COALESCE(pipa.SaleAdjustments, 0)) OVER(PARTITION BY woos.ItemID ORDER BY woos.Date
                ROWS BETWEEN 29 PRECEDING AND CURRENT ROW) AS S30Temp,
            CAST(
                CASE
                    WHEN
                        [1421 Kingbird - OOS] = 1 AND
                        [2610 West L - OOS] = 1 AND
                        [4021 N 27th - OOS] = 1 AND
                        [Nevada - OOS] = 1 AND
                        [1000 Commerce - OOS] = 1 AND
                        [Pennsylvania - OOS] = 1 AND
                        [Resell - OOS] = 1 AND
                        [1001 W Upland - OOS] = 1 AND
                        [Resell - OOS] = 1 AND
                        [Nevada Marion - OOS] = 1 AND
                        [NV FC Transfers - OOS] = 1 AND
                        [Indiana FC - OOS] = 1 AND
                        [Nevada Lamb - OOS] = 1 AND
                        [Parts - OOS] = 1 AND
                        [Nevada Offsite Storage - OOS] = 1 AND
                        [Pennsylvania Overflow - OOS] = 1 AND
                        [Nevada Ann - OOS] = 1 AND
                        [Georgia - OOS] = 1 AND
                        [Texas Wintergreen - OOS] = 1 AND
                        [NE FC Transfers - OOS] = 1 AND
                        [GA FC Transfers - OOS] = 1 AND
                        [TX FC Transfers - OOS] = 1 AND
                        [IN FC Transfers - OOS] = 1 AND
                        [PA FC Transfers - OOS] = 1 AND
                        [Indiana Greenwood - OOS] = 1 AND
                        [Pennsylvania Nanticoke - OOS] = 1
                        THEN 1
                    ELSE 0
                END
            AS BIT) [Network - OOS]
        FROM temp.WarehousePivot woos
        LEFT OUTER JOIN temp.ProductInitialPullAgg pipa ON pipa.ItemID = woos.ItemID AND
                                                           pipa.AdjustmentDate = woos.Date
        LEFT OUTER JOIN temp.SupplyChainStatus scs ON scs.ItemID = woos.ItemID AND
                                                      scs.Date = woos.Date
        LEFT OUTER JOIN SaleDates sd ON sd.ItemID = woos.ItemID AND
                                        woos.Date >= sd.SaleAdjustmentDate AND
                                        woos.Date < sd.NextSaleDate
        LEFT OUTER JOIN temp.ItemLimiter minAdjust ON minAdjust.ItemID = woos.ItemID
    )
    INSERT INTO temp.RollingSales
    SELECT
        Date,
        SaleAdjustmentDate,
        ItemID,
        SupplyChainStatusNodeID,
        ItemStatusID,
        InventoryStatusID,
        ProcurementStatusID,
        SaleOOSEligible,
        DaysSinceRCVOOSEligible,
        InventoryStatusOOSEligible,
        [Network - Daily Unit Sales],
        CAST(CASE WHEN S365Temp < 0 THEN 0 ELSE S365Temp END AS INT) S365,
        CAST(CASE WHEN S90Temp < 0 THEN 0 ELSE S90Temp END AS INT) S90,
        CAST(CASE WHEN S30Temp < 0 THEN 0 ELSE S30Temp END AS INT) S30,
        [Network - OOS]
    FROM TempRoll

    CREATE NONCLUSTERED INDEX temp_RollingSales ON temp.RollingSales(Date);
    CREATE CLUSTERED COLUMNSTORE INDEX cci ON temp.RollingSales;
    CREATE NONCLUSTERED INDEX temp_RollingSales_S30_S90_S365 ON temp.RollingSales(S30, S90, S365);

END



CREATE OR ALTER PROCEDURE ssis.usp_Update_temp_SupplyChainStatusBase

AS

BEGIN

    DROP TABLE IF EXISTS temp.SupplyChainStatusBase;
    CREATE TABLE temp.SupplyChainStatusBase (
        Date DATE,
        ItemID INT
    );

    INSERT INTO temp.SupplyChainStatusBase
    SELECT CAST(CAST(DateID AS NVARCHAR) AS DATE) Date,
        ItemID
    FROM lookup.Date d
    CROSS JOIN (
        SELECT DISTINCT lsi.ItemID
        FROM stage.LSInventory lsi
        JOIN temp.ItemLimiter limit ON limit.ItemID = lsi.ItemID
    ) item

    CREATE CLUSTERED INDEX temp_SupplyChainStatusBase ON temp.SupplyChainStatusBase(ItemID, Date);
    CREATE NONCLUSTERED INDEX temp_SupplyChainStatusBase_Date ON temp.SupplyChainStatusBase(Date);

END